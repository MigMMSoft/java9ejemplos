package com.previred.java.presentacionjava9.intefacePrivate;

/**
 *
 * @author MigSoft
 */
public interface CalculadorInterface {
    public Integer getValorA();
    public Integer getValorB();
    
    default int suma(){
       return calcula(1); 
    }
    
    default int resta(){
        return calcula(2);
    }
    
    /**
     * Ejemplo de metodo privado en interfaces
     * @param accion
     * @return 
     */
    private Integer calcula(int accion){
        if(getValorA() != null && getValorB() != null){
            switch (accion) {
                case 1:
                    return getValorA() + getValorB();
                case 2:
                    return getValorA() - getValorB();
                default:
                    return 0;
            }
        }else{
            return 0;
        }
    }
}
