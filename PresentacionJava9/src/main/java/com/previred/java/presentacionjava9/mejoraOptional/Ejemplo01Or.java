package com.previred.java.presentacionjava9.mejoraOptional;

import java.util.List;
import java.util.Optional;

/**
 *
 * @author MigSoft
 */
public class Ejemplo01Or {
    public static void main(String args[]) {
        Ejemplo01Or ejemplo1 = new Ejemplo01Or();
        ejemplo1.listaEvaluacion();
    }
    
    public void listaEvaluacion(){
        List<Optional<String>> days = List.of(Optional.of("Monday"),
                Optional.of("Lunes"),
                Optional.empty(),
                Optional.ofNullable(null),
                Optional.of("Jueves"),
                Optional.of("Viernes"),
                Optional.of("Jueves")
        );
        //Se define un opción para el dato nulo o vacío
        Optional<String> defaultValue = Optional.of("sin dia");
        /**
         * Optional<T>	or​(Supplier<? extends Optional<? extends T>> supplier)	
         * Si hay un valor presente, devuelve un Opcional que describe el valor, 
         * de lo contrario devuelve un Opcional producido por la función de suministro.
         */
        days.stream().forEach(dias -> 
                System.out.println("Valor:" + dias.or(() -> defaultValue)));
    }
}
