package com.previred.java.presentacionjava9.intefacePrivate;

/**
 *
 * @author MigSoft
 */
public class Ejemplo01Private {
    public static void main(String args[]) {
        Calulador calulador = new Calulador(10,3);
        
        System.out.println("Suma: " + calulador.suma());
        System.out.println("Resta: " + calulador.resta());
    }
}

class Calulador implements CalculadorInterface{
    private final int valorA;
    private final int valorB;
    
    public Calulador(int valorA, int valorB){
        this.valorA = valorA;
        this.valorB = valorB;
    }

    @Override
    public Integer getValorA() {
        return valorA;
    }

    @Override
    public Integer getValorB() {
        return valorB;
    }
}