package com.previred.java.presentacionjava9.mejoraStream;

import java.util.stream.Stream;

/**
 *
 * @author MigSoft
 */
public class Ejemplo01TakeWhile {

    public static void main(String args[]) {
        System.err.println("---- Primera lista ----");
        Stream<Integer> stdMarks = Stream.of(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
        Stream<Integer> ngMarks = stdMarks.takeWhile(s -> s < 5);

        ngMarks.forEach(System.out::println);
        
        System.err.println("---- Segunda lista Desordenada ----");
        Stream<Integer> givenMarks = Stream.of(1, 2, 10, 9, 5, 6, 2, 3);
        Stream<Integer> remainMarks = givenMarks.takeWhile(s -> s < 5);

        remainMarks.forEach(System.out::println);
        
        System.err.println("---- Segunda lista Ordenada ----");
        Stream<Integer> givenMarksSort = Stream.of(1, 2, 10, 9, 5, 6, 2, 3);
        Stream<Integer> remainMarksSort = givenMarksSort.sorted().takeWhile(s -> s < 5);

        remainMarksSort.forEach(System.out::println);
    }
}
