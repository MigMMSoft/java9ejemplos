package com.previred.java.presentacionjava9.mejoraOptional;

import java.util.List;
import java.util.Optional;

/**
 *
 * @author MigSoft
 */
public class Ejemplo02ifPresentOrElse {

    public static void main(String args[]) {
        Ejemplo02ifPresentOrElse ejemplo2 = new Ejemplo02ifPresentOrElse();
        ejemplo2.listaEvaluacion();
    }

    public void listaEvaluacion() {
        List<Optional<String>> days = List.of(Optional.of("Monday"),
                Optional.of("Lunes"),
                Optional.empty(),
                Optional.ofNullable(null),
                Optional.of("Jueves"),
                Optional.of("Viernes"),
                Optional.of("Jueves")
        );
        
        /**
         * void	ifPresentOrElse​(Consumer<? super T> action, Runnable emptyAction)
         * Si hay un valor presente, realiza la acción dada con el valor, de lo 
         * contrario lleva a cabo la acción dada basada en vacío.
         */
        days.stream().forEach(p -> p.ifPresentOrElse(System.out::println,
                () -> System.out.println("Registra Sin dia")));
    }
}
