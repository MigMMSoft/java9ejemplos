package com.previred.java.presentacionjava9.pruebaProceso;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 *
 * @author MigSoft
 */
public class ProcesaArchivo {
    private static final int VECTOR = 6;
    private static final int MAX_VECTOR = 23;
    private static final int INICIO_CADENA = 9;
    private static final int LARGO_TOTAL = 148;
    
    //
    public static void main(String args[]) throws IOException {
        ProcesaArchivo p = new ProcesaArchivo();
        //p.procesar();
        String arch = "..\\..\\vector1000000.txt";
        Long inicio = System.nanoTime();
        p.procesaArchivo(arch);
        Long fin = System.nanoTime();
        System.out.println("Tiempo ocupado: " + TimeUnit.SECONDS.convert((fin-inicio), TimeUnit.NANOSECONDS));
    }
    
    public void procesaArchivo(String archivo) throws IOException{
        File salida = new File("procesado"+archivo);
        try (BufferedWriter output = new BufferedWriter(new FileWriter(salida))) {
            Consumer<String> c = (linea) -> {
                try {
                    output.write(linea);
                } catch (IOException ex) {
                    Logger.getLogger(ProcesaArchivo.class.getName()).log(Level.SEVERE, null, ex);
                }
            };
            
            Path path = FileSystems.getDefault().getPath(archivo);
            Files.lines(path)
                //.parallel()
                .map(ProcesaArchivo::procesarLine)
                .forEach(c);
        }
        
    }
    
    public static String procesarLine(String linea){
        //rescata secuencia
        StringBuilder salida = new StringBuilder(linea.substring(0, INICIO_CADENA));
        //divide periodos
        int inicio = INICIO_CADENA;
        int fin = INICIO_CADENA + VECTOR;
        ArrayList<String> list = new ArrayList();
        while (linea.length() > fin) {
            String fecha = linea.substring(inicio, fin);
            inicio += VECTOR;
            fin += VECTOR;
            list.add(fecha);
        }
        //filtra y junta
        List<String> slist = list
                .stream()
                .filter(p->!"000000".equals(p))
                .distinct()
                .collect(Collectors.toList());
        //califica
        long elementos = slist.size();
        if (elementos == 0) {//sin elementos
            salida.append("N");
        }else if(elementos > MAX_VECTOR){
            salida.append("S");
        }else{
            salida.append("D");
            salida.append(slist.stream().sorted().collect(Collectors.joining()));
        }
        //rellena
        while (salida.length() < LARGO_TOTAL) {
            salida.append(" ");
        }

        return salida.append("\n").toString();
    }
}
