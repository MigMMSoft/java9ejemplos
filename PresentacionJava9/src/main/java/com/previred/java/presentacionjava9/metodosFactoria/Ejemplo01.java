package com.previred.java.presentacionjava9.metodosFactoria;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author MigSoft
 */
public class Ejemplo01 {
    
    public static void main(String args[]) {
        Ejemplo01 ejemplo = new Ejemplo01();
        
        ejemplo.java8();
        ejemplo.java9();
    }

    public void java8() {
        List<String> list = Collections.unmodifiableList(Arrays.asList("a", "b", "c"));
        Set<String> set = Collections.unmodifiableSet(new HashSet<>(Arrays.asList("a", "b", "c")));

        Map<String, Integer> map = new HashMap<>();
        map.put("a", 1);
        map.put("b", 2);
        map.put("c", 3);
        map = Collections.unmodifiableMap(map);
        
        //Error al tratar de agregar datos a listas inmutables
//        list.add("d");
//        set.add("d");
//        map.put("d", 4);
        
        System.out.println("---- Lista ----");
        list.stream().forEach(System.out::println);
        System.out.println("---- Set ----");
        for (Iterator iterator = set.iterator(); iterator.hasNext();) {
            System.out.println("Set: " + iterator.next());
        }
        System.out.println("---- Mapa ----");
        map.values().stream().forEach(System.out::println);
    }

    public void java9() {
        List<String> list = List.of("a", "b", "c");
        Set<String> set = Set.of("a", "b", "c");
        Map<String, Integer> map = Map.of("a", 1);
        
        //Error al tratar de agregar datos a listas inmutables
//        list.add("d");
//        set.add("d");
//        map.put("d", 4);
//        
        System.out.println("---- Lista ----");
        list.stream().forEach(System.out::println);
        System.out.println("---- Set ----");
        for (Iterator iterator = set.iterator(); iterator.hasNext();) {
            System.out.println("Set: " + iterator.next());
        }
        System.out.println("---- Mapa ----");
        map.values().stream().forEach(System.out::println);
    }
}
