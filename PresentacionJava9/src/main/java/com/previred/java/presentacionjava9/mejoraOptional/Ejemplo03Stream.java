package com.previred.java.presentacionjava9.mejoraOptional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 *
 * @author MigSoft
 */
public class Ejemplo03Stream {
    public static void main(String args[]) {
        Ejemplo03Stream ejemplo3 = new Ejemplo03Stream();
        ejemplo3.listaEvaluacion();
    }
    
    
    public void listaEvaluacion() {
        Optional<String> value = Optional.of("Clase opcional nos permite tratar la instancia como un flujo.");
        
        /**
         * Stream<T>	stream​()
         * Si hay un valor presente, devuelve un flujo secuencial que contiene 
         * solo ese valor, de lo contrario devuelve un flujo vacío.
         */
        value.stream().forEach(System.out::println);
        List<String> collect = value.stream().map(String::toUpperCase).collect(Collectors.toList());
        
        collect.stream().forEach(System.out::println);
    }
}
