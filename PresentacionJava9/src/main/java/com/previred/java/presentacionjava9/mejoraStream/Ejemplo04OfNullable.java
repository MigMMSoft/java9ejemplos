package com.previred.java.presentacionjava9.mejoraStream;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

/**
 *
 * @author MigSoft
 */
public class Ejemplo04OfNullable {
    public static void main(String args[]) {
       List<Integer> numbers = Arrays.asList(2, 4, null, 8, 9, 10, null);
       
       System.out.println("---- Evalúa  lista con datos ----");
       Stream.ofNullable(numbers).forEach(System.out::println);
       
       numbers = null;
       System.out.println("---- Evalúa  lista nula ----");
       Stream.ofNullable(numbers).forEach(System.out::println);

    }
}
