package com.previred.java.presentacionjava9.mejoraStream;

import java.util.stream.IntStream;

/**
 *
 * @author MigSoft
 */
public class Ejemplo03Iterate {

    public static void main(String args[]) {
        Ejemplo03Iterate ejemplo = new Ejemplo03Iterate();
       
        System.out.println("Suma 1000 multiplos de 3: " + 
                (ejemplo.sumaMultiplos(3,1000)));
        
        System.out.println("Suma 1000 multiplos de 5: " + 
                (ejemplo.sumaMultiplos(5,1000)));
        
        System.out.println("Suma 1000 multiplos de 3 y 5: " + 
                ejemplo.sumaMultiplos(3,5,1000));
    }
    
    public long sumaMultiplos(int multiplo, int iteraciones){
      return sumaMultiplos(multiplo,multiplo,iteraciones);
    }
    
    public long sumaMultiplos(int multiplo1, int multiplo2, int iteraciones){
      return IntStream.iterate(1, i -> i<iteraciones, i -> i + 1)
              .filter(n -> n % multiplo1 == 0 || n % multiplo2 == 0)
              .count();
    }
}
