package com.previred.java.presentacionjava9.varios;

import java.util.Arrays;

/**
 *
 * @author MigSoft
 */
public class Ejemplo04MismatchArray {

    public static void main(String args[]) {
        int[] a = {1, 2, 3, 4, 5};
        int[] b = {1, 2, 3, 4, 5};
        int[] c = {1, 2, 4, 4, 5, 6};
        
        /**
         * Nos muestra el índice en la lista cuando encuentra una diferencia
         * Retorna -1 si las listas a comparar son iguales
         */

        System.out.println("Iguales: " + Arrays.mismatch(a, b));
        System.out.println("Distintos: " + Arrays.mismatch(a, c));
        System.out.println("Iguales 0-2 0-2: " + Arrays.mismatch(a, 0, 2, c, 0, 2));
        System.out.println("Distintos 0-3 0-3: " + Arrays.mismatch(a, 0, 3, c, 0, 3));

        //mismatch return relative index
        System.out.println("Indice 0 distinto: " + Arrays.mismatch(a, 2, 5, c, 2, 5));
        System.out.println("Indice 1 distinto: " + Arrays.mismatch(a, 1, 3, c, 1, 3));
    }
}
