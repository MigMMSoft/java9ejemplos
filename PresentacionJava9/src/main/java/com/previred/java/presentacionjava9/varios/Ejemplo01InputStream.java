package com.previred.java.presentacionjava9.varios;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 *
 * @author MigSoft
 */
public class Ejemplo01InputStream {
    public static void main(String args[]) throws IOException {
                byte [] inBytes = "Hello Java 9".getBytes();
        InputStream bis = new ByteArrayInputStream(inBytes);
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
 
        try (bis; bos) {
            //Trasfiere input a output stream
            bis.transferTo(bos);
 
            System.err.println("OUT: " + bos.toString());
        } 
    }
}
