package com.previred.java.presentacionjava9.mejoraStream;

import java.util.stream.Stream;

/**
 *
 * @author MigSoft
 */
public class Ejemplo02DropWhile {

    public static void main(String args[]) {
        System.err.println("---- Primera lista ----");
        Stream<Integer> stdMarks = Stream.of(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
        Stream<Integer> goodMarks = stdMarks.dropWhile(s -> s < 5);

        goodMarks.forEach(System.out::println);
        System.err.println("---- Segunda lista desordenada ----");
        Stream<Integer> givenMarks = Stream.of(1, 2, 10, 9, 5, 6, 2, 3);
        Stream<Integer> remainMarks = givenMarks.dropWhile(s -> s < 5);
        
        remainMarks.forEach(System.out::println);
        System.err.println("---- Segunda lista ordenada ----");
        Stream<Integer> givenMarksSort = Stream.of(1, 2, 10, 9, 5, 6, 2, 3);
        Stream<Integer> remainMarksSort = givenMarksSort.sorted().dropWhile(s -> s < 5);
        
        remainMarksSort.forEach(System.out::println);
    }
}
