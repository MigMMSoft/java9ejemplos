package com.previred.java.presentacionjava9.varios;

import java.util.Arrays;

/**
 *
 * @author MigSoft
 */
public class Ejemplo03compareArray {

    public static void main(String args[]) {
        int[] lista1 = {5, 6, 7, 8, 9, 10};
        int[] lista2 = {5, 6, 7, 10, 9, 10};
        int[] lista3 = {5, 6, 7, 8};

        System.out.println("Com 0-3 y 0-3: " + Arrays.
                compare(lista1, 0, 3,
                        lista2, 0, 3));

        System.out.println("Com 0-4 y 0-x: " + Arrays.
                compare(lista1, 0, 4,
                        lista3, 0, 4));

        System.out.println("Com 0-4 y 0-x: " + Arrays.
                compare(lista2, 0, 4,
                        lista3, 0, 4));
    }
}
