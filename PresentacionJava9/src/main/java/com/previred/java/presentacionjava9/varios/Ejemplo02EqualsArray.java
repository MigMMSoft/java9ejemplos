package com.previred.java.presentacionjava9.varios;

import java.util.Arrays;

/**
 *
 * @author MigSoft
 */
public class Ejemplo02EqualsArray {

    public static void main(String args[]) {
        int[] lista1 = {0, 1, 2, 3, 4, 5};
        int[] lista2 = {3, 4, 5, 1, 2, 0};

        System.out.println("Diferentes: " + Arrays.equals(lista1, lista2));
        
        System.out.println("Dif 1-3 y 1-3: " + Arrays.equals(lista1, 1, 3, lista2, 1, 3));
        System.out.println("Dif 1-3 y 3-5: " + Arrays.equals(lista1, 1, 3, lista2, 3, 5));
        System.out.println("Dif 3-5 y 0-2: " + Arrays.equals(lista1, 3, 5, lista2, 0, 2));
    }
}
