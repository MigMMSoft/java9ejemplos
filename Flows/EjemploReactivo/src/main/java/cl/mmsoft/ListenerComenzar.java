package cl.mmsoft;

import cl.mmsoft.view.ControllerDetalleEntrada;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.SubmissionPublisher;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author mig_s
 */
public class ListenerComenzar implements ActionListener {

    private static final long MAX_SECONDS_TO_KEEP_IT_WHEN_NO_SPACE = 2;
    private static final Logger LOG
            = LoggerFactory.getLogger(ListenerComenzar.class);
    private final int maximoEnPO;
    private final int maximoAlmacenamientoEnPO;
    private final ControllerDetalleEntrada controlDetalle;
    private final SubmissionPublisher<Integer> publicador;
    private final long tiempoDeJack;
    private final long tiempoPete;

    public ListenerComenzar(final long tiempoDeJack,
            final long tiempoPete,
            final int maximoEnPO,
            final int maximoAlmacenamientoEnPO,
            ControllerDetalleEntrada controlDetalle) {
        publicador = new SubmissionPublisher<>(ForkJoinPool.commonPool(), maximoAlmacenamientoEnPO);
        this.maximoEnPO = maximoEnPO;
        this.maximoAlmacenamientoEnPO = maximoAlmacenamientoEnPO;
        this.controlDetalle = controlDetalle;
        this.tiempoDeJack = tiempoDeJack;
        this.tiempoPete = tiempoPete;

        final MagazineSubscriber jack = new MagazineSubscriber(
                tiempoDeJack,
                MagazineSubscriber.JACK,
                controlDetalle
        );

        final MagazineSubscriber pete = new MagazineSubscriber(
                tiempoPete,
                MagazineSubscriber.PETE,
                controlDetalle
        );

        publicador.subscribe(jack);
        publicador.subscribe(pete);

        controlDetalle.setProgresoEmisor(0);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        LOG.info("Imprimiendo " + maximoEnPO + " revistas por suscriptor, con espacio en editor para "
                + maximoAlmacenamientoEnPO + ". Ellos tienen " + MAX_SECONDS_TO_KEEP_IT_WHEN_NO_SPACE
                + " segundos para consumir cada revista.");
        Thread thread = new Thread("New Thread") {
            public void run() {
                try {
                    IntStream.rangeClosed(1, maximoEnPO).forEach((number) -> {
                        LOG.info("Revista de ofrecimiento " + number + " a los consumidores");
                        controlDetalle.setProgresoEmisor(number);
                        final int lag = publicador.offer(
                                number,
                                MAX_SECONDS_TO_KEEP_IT_WHEN_NO_SPACE,
                                TimeUnit.SECONDS,
                                (subscriber, msg) -> {
                                    subscriber.onError(
                                            new RuntimeException("Oye " + ((MagazineSubscriber) subscriber)
                                                    .getSubscriberName() + "!Eres demasiado lento para conseguir revistas"
                                                    + " y no tenemos más espacio para ellos.! "
                                                    + "Voy a dejar tu revista: " + msg));
                                    return false; // don't retry, we don't believe in second opportunities
                                });
                        if (lag < 0) {
                            log("Calleron " + -lag + " revistas");
                        } else {
                            log("El consumidor más lento tiene " + lag
                                    + " revistas en total para ser recogidos");
                        }
                    });

                    // Blocks until all subscribers are done (this part could be improved
                    // with latches, but this way we keep it simple)
                    while (publicador.estimateMaximumLag() > 0) {
                        Thread.sleep(500L);
                    }

                    // Closes the publisher, calling the onComplete() method on every subscriber
                    publicador.close();
                    // give some time to the slowest consumer to wake up and notice
                    // that it's completed
                    Thread.sleep(Math.max(tiempoDeJack, tiempoPete));
                } catch (InterruptedException ex) {
                    LOG.error("Error en ejecucion: ", ex);
                }
            }
        };

        thread.start();
    }

    private static void log(final String message) {
        LOG.info("===========> " + message);
    }
}
