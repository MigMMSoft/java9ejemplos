package cl.mmsoft.view;

import javax.swing.BoundedRangeModel;
import javax.swing.DefaultBoundedRangeModel;
import javax.swing.DefaultButtonModel;

/**
 *
 * @author mig_s
 */
public class ModelDetalleEntrega {
    TextModel totalPublicado = new TextModel();
    TextModel totalPorPublicar = new TextModel();
    TextModel totalAlmacen = new TextModel();
    Integer progresoEmisorInt = 0;
    BoundedRangeModel progresoEmisor = new DefaultBoundedRangeModel();
    TextModel nombreSubcriptor1 = new TextModel();
    TextModel totalAlmacenado1= new TextModel();
    TextModel revistas1= new TextModel();
    BoundedRangeModel progresoReseptor1 = new DefaultBoundedRangeModel();
    TextModel nombreSubcriptor2 = new TextModel();
    BoundedRangeModel progresoReseptor2 = new DefaultBoundedRangeModel();
    TextModel totalAlmacenado2= new TextModel();
    TextModel revistas2= new TextModel();
    DefaultButtonModel comenzar = new DefaultButtonModel();

    public BoundedRangeModel getProgresoEmisor() {
        return progresoEmisor;
    }

    public TextModel getNombreSubcriptor1() {
        return nombreSubcriptor1;
    }

    public BoundedRangeModel getProgresoReseptor1() {
        return progresoReseptor1;
    }

    public TextModel getNombreSubcriptor2() {
        return nombreSubcriptor2;
    }

    public BoundedRangeModel getProgresoReseptor2() {
        return progresoReseptor2;
    }

    public TextModel getTotalPublicado() {
        return totalPublicado;
    }

    public TextModel getTotalPorPublicar() {
        return totalPorPublicar;
    }

    public TextModel getTotalAlmacen() {
        return totalAlmacen;
    }

    public TextModel getTotalAlmacenado1() {
        return totalAlmacenado1;
    }

    public TextModel getTotalAlmacenado2() {
        return totalAlmacenado2;
    }

    public Integer getProgresoEmisorInt() {
        return progresoEmisorInt;
    }

    public void setProgresoEmisorInt(Integer totalAlmacenInt) {
        this.progresoEmisorInt = totalAlmacenInt;
    }

    public TextModel getRevistas1() {
        return revistas1;
    }

    public TextModel getRevistas2() {
        return revistas2;
    }

    public DefaultButtonModel getComenzar() {
        return comenzar;
    }

    public void setComenzar(DefaultButtonModel comenzar) {
        this.comenzar = comenzar;
    }
}
