package cl.mmsoft.view;

import java.awt.event.ActionListener;
import javax.swing.JPanel;
import javax.swing.text.BadLocationException;

/**
 *
 * @author mig_s
 */
public class ControllerDetalleEntrada {
    private ModelDetalleEntrega model;
    private ViewDetalleEntrega view;
    
    public ControllerDetalleEntrada(Integer maxEmisor, Integer maxAlmacen) throws BadLocationException{
        this.model = new ModelDetalleEntrega();
        this.view = new ViewDetalleEntrega();
        view.setModel(model);
        
        this.model.getProgresoEmisor().setMinimum(0);
        this.model.getProgresoEmisor().setMaximum(maxEmisor);
        this.model.getTotalPorPublicar().setText(maxEmisor.toString());
        this.model.getProgresoEmisor().setValue(0);
        
        this.model.getProgresoReseptor1().setMaximum(maxEmisor);
        this.model.getProgresoReseptor2().setMaximum(maxEmisor);
        
        this.model.getTotalAlmacen().setText(maxAlmacen.toString());
        
        this.model.getRevistas1().setText(" ");
        this.model.getRevistas2().setText(" ");
    }
    
    public void addAccionComenzar(ActionListener accion){
        this.model.getComenzar().addActionListener(accion);
    }
    
    public int addProgreso(String nombre){
        int indice = 0;
        if(this.model.getNombreSubcriptor1().getText() == null){
            indice = 1;
            this.model.getNombreSubcriptor1().setText(nombre);
            this.model.getProgresoReseptor1().setMinimum(0);
            this.model.getProgresoReseptor1().setValue(0);
        } else if(this.model.getNombreSubcriptor2().getText() == null){
            indice = 2;
            this.model.getNombreSubcriptor2().setText(nombre);
            this.model.getProgresoReseptor2().setMinimum(0);
            this.model.getProgresoReseptor2().setValue(0);
        }
        
        return indice;
    }
    
    public void setProgresoSubscripto(Integer valor, Integer indice){
        Integer total = this.model.getProgresoEmisorInt() - valor;
        String valorStr = " " + valor;
        if(indice == 1){
            this.model.getProgresoReseptor1().setValue(valor);
            this.model.getTotalAlmacenado1().setText(total.toString());
            this.model.getRevistas1().setText(this.model.getRevistas1().getText() + valorStr);
        }else{
            this.model.getProgresoReseptor2().setValue(valor);
            this.model.getTotalAlmacenado2().setText(total.toString());
            this.model.getRevistas2().setText(this.model.getRevistas2().getText() + valorStr);
        }
    }
   
    public void setProgresoEmisor(Integer valor){
        this.model.getProgresoEmisor().setValue(valor);
        this.model.getTotalPublicado().setText(valor.toString());
        this.model.setProgresoEmisorInt(valor);
    }
    
    public void setAlmacenado(Integer valor){
        this.model.getTotalAlmacen().setText(valor.toString());
    }
    
    public JPanel getPanel(){
        return this.view;
    }
}
