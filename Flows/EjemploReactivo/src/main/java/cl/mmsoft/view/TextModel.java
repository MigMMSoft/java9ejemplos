package cl.mmsoft.view;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

/**
 *
 * @author mig_s
 */
public class TextModel extends PlainDocument{
    public void setText(String text){
        try {
            if(this.getLength() != 0){
                remove(0, this.getLength());
            }
            insertString(0, text, null);
        } catch (BadLocationException ex) {
            Logger.getLogger(TextModel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public String getText(){
        if(this.getLength() == 0){
            return null;
        }else{
            try {
                return this.getText(0, this.getLength());
            } catch (BadLocationException ex) {
                Logger.getLogger(TextModel.class.getName()).log(Level.SEVERE, null, ex);
                return null;
            }
        }
    }
}
