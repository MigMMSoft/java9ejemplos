package cl.mmsoft;

import cl.mmsoft.view.ControllerDetalleEntrada;
import cl.mmsoft.view.ViewFlow;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author mig_s
 */
public class RunReactiveApp {

    private static final Logger LOG
            = LoggerFactory.getLogger(RunReactiveApp.class);

    public static void main(String[] args) throws Exception {
        ViewFlow viewFlow = new ViewFlow();
        viewFlow.setVisible(true);
        
        LOG.info("\n\n### CASO 1: Los suscriptores son rápidos, el tamaño del "
                + "búfer no es tan importante en este caso..");
        ControllerDetalleEntrada control1 = new ControllerDetalleEntrada(20,8);
        viewFlow.addDetalle(control1.getPanel());
        control1.addAccionComenzar(new ListenerComenzar(300L, 1000L, 20, 8, control1));

        LOG.info("\n\n### CASO 2: Un suscriptor lento, pero un buffer suficientemente "
                + "bueno tamaño en el lado del editor para mantener todos los "
                + "artículos hasta que sean recogidos");
        ControllerDetalleEntrada control2 = new ControllerDetalleEntrada(20,20);
        viewFlow.addDetalle(control2.getPanel());
        control2.addAccionComenzar(new ListenerComenzar(1000L, 3000L, 20, 20, control2));

        LOG.info("\n\n### CASO 3: Un suscriptor lento, y un buffer muy limitado. "
                + "tamaño en el lado del editor por lo que es importante mantener "
                + "el lento suscriptor bajo control");
        ControllerDetalleEntrada control3 = new ControllerDetalleEntrada(20,8);
        viewFlow.addDetalle(control3.getPanel());
        control3.addAccionComenzar(new ListenerComenzar(1000L, 3000L, 20, 8, control3));
    }
}
