package cl.mmsoft;

import cl.mmsoft.view.ControllerDetalleEntrada;
import java.util.concurrent.Flow;
import java.util.stream.IntStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MagazineSubscriber implements Flow.Subscriber<Integer> {

  public static final String JACK = "Jack";
  public static final String PETE = "Pete";

  private static final Logger LOG = LoggerFactory.
    getLogger(MagazineSubscriber.class);

  private final long sleepTime;
  private final String subscriberName;
  private Flow.Subscription subscription;
  private int nextMagazineExpected;
  private int totalRead;
  
  private final int indice;
  private final ControllerDetalleEntrada controlDetalle;

  MagazineSubscriber(final long sleepTime, 
          final String subscriberName,
          ControllerDetalleEntrada controlDetalle) {
    this.sleepTime = sleepTime;
    this.subscriberName = subscriberName;
    this.nextMagazineExpected = 1;
    this.totalRead = 0;
    
    this.indice = controlDetalle.addProgreso(subscriberName);
    this.controlDetalle = controlDetalle;
  }

  @Override
  public void onSubscribe(final Flow.Subscription subscription) {
    this.subscription = subscription;
    subscription.request(1);
  }

  @Override
  public void onNext(final Integer magazineNumber) {
    if (magazineNumber != nextMagazineExpected) {
      IntStream.range(nextMagazineExpected, magazineNumber).forEach(
        (msgNumber) ->
          log("¡Oh no! Me perdí la revista " + msgNumber)
      );
      // Catch up with the number to keep tracking missing ones
      nextMagazineExpected = magazineNumber;
    }
    this.controlDetalle.setProgresoSubscripto(magazineNumber, this.indice);
    log("¡Genial! Tengo una nueva revista: " + magazineNumber);
    takeSomeRest();
    nextMagazineExpected++;
    totalRead++;

    log("Conseguiré otra revista ahora, la siguiente debería ser: " +
      nextMagazineExpected);
    subscription.request(1);
  }

  @Override
  public void onError(final Throwable throwable) {
    log("Vaya, recibí un error del editor: " + throwable.getMessage());
    LOG.error("Error Capturado: ", throwable);
  }

  @Override
  public void onComplete() {
    log("¡Finalmente! Completé la suscripción, obtuve en total. " +
      totalRead + " revistas.");
  }

  private void log(final String logMessage) {
    LOG.info("<=========== [" + subscriberName + "] : " + logMessage);
  }

  public String getSubscriberName() {
    return subscriberName;
  }

  private void takeSomeRest() {
    try {
      Thread.sleep(sleepTime);
    } catch (InterruptedException e) {
      throw new RuntimeException(e);
    }
  }
}