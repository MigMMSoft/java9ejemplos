package com.logicbig.example;

import java.util.logging.Logger;
import java.util.logging.Level;

public class Test {
  private static final Logger LOGGER = Logger.getLogger(Test.class.getName());

  public static void main(String[] args) {
      LOGGER.info("Running test application..");
      ProcessBuilder pb = new ProcessBuilder("cmd", "/c", "path");
        try {
            Process p = pb.start();
            long pid = p.pid(); 
            LOGGER.log(Level.INFO, "PID: {0}", pid);
        } catch (Exception ex) {
            LOGGER.log(Level.SEVERE, "Error PID", ex);
        }
  }
}