### Ejemplo Java 9

Proyecto de ejemplo de nuevas funcionalidades en java 9
Estos ejemplos son para la presentación que se encuentra en:
[Prezi java 9-10-11](https://prezi.com/view/RT84iTZK97WgbScY9b5R)

## Detalle de los sistemas

Java 9
Maven 3

## Detalle de Directorios

Los ejemplos contenidos están organizados de la siguiente forma:

 ## Java 9 (directorio Java9)
 Ejemplos de nuevas funcionalidades con Java 9

 # **Flows** 
 Ejemplo de programación reactiva usando Flow. App Swing que muestra la ejecución utilizando encolamiento con Flows.
 	* EjemploReactivo
 		*cl.mmsoft.RunReactiveApp (clase main)

 **PresentacionJava9**
 Ejemplos varios de nuevas implementaciones, cada clase tiene su implementación de main class (\src\main\java\com\previred\java\presentacionjava9)
 	* intefacePrivate
 		* Ejemplo01Private.java - Ejemplo de métodos privados en interfaces
 	* mejoraOptional
 		* Ejemplo01Or.java - Ejemplo de uso de **or** en **Optional**
 		* Ejemplo02ifPresentOrElse.java - Ejemplo de uso de **ifPresentOrElse** en **Optional**
 		* Ejemplo03Stream.java - Ejemplo de incorporación de **stream** en **Optional**
 	* mejoraStream
 		* Ejemplo01TakeWhile.java - Ejemplo de **TakeWhile** en **stream**
 		* Ejemplo02DropWhile.java - Ejemplo de **DropWhile** en **stream**
 		* Ejemplo03Iterate.java – Corrección de **iteraror** de **java 8**
 		* Ejemplo04OfNullable.java - Ejemplo de **OfNullable** en **stream**
 	* metodosFactoria
 		* Ejemplo01.java - Ejemplo de creación de listas inmutables
 	* varios
 		* Ejemplo01InputStream.java - Ejemplo de nuevo uso de Input y Output Stream
 		* Ejemplo02EqualsArray.java - Ejemplo de **equals** en **array**
 		* Ejemplo03compareArray.java - Ejemplo de **compare** en **array**
 		* Ejemplo04MismatchArray.java - Ejemplo de **Mismatch** en **array**
 	* pruebaProceso
 		* ProcesaArchivo.java - Este es un ejemplo de procesamiento de un archivo de 1 millón de registros, esto es para comparar velocidad de procesamiento entre versiones de java


 **PresentacionJava9Complementos**
 Ejemplo de modularidad con JLink (\jlink-example). Este ejemplo empaqueta una aplicacion con sus modulos java, generando una aplicacion java 9 portable.

 	* \src\module-info.java - Definición de los módulos que componen esta aplicación
 	* \src\com\logicbig\example\Test.java - Ejemplo básico de popularización
 	* java9.bat - Compila y genera los módulos
 	* \out\run.bat - Ejecuta el programa desde el modulo